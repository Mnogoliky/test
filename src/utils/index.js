/**
 *
 * @param {string} type
 * @param {function} funBody
 * @return {function(...[*]): {type: object}}
 */
export const createAction = (type, funBody) => {
	const newFun = (...props) => ({type, ...funBody(...props)});
	newFun.toString = function () {return type};
	return newFun;
};

/**
 *
 * @param {number | string | Date} innerdate
 * @param {bool} withseconds
 * @return {string}
 */
export const getTimeFromDate = (innerdate, withseconds) => {
	const date = new Date(innerdate);
	if (withseconds)
		return `${ date.getHours() }:${ date.getMinutes().toString().padStart(2, '0') }:${ date.getSeconds().toString().padStart(2, '0') }`;
	else
		return `${ date.getHours() }:${ date.getMinutes().toString().padStart(2, '0') }`;
};

/**
 *
 * @return {string} date
 */
export const getDateStr = () => {
	const date = new Date();
	const monthes = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December',
	];
	return `${date.getDay()} ${monthes[date.getMonth()]} ${date.getFullYear()}`;
};