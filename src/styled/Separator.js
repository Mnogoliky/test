import styled from 'styled-components'
import PropTypes from 'prop-types'

export const Separator = styled.div`
	flex-direction: ${props => props.horizontally ? 'column' : 'row'};
	display: flex;
	justify-content: space-between;
`;

Separator.PropTypes = {
	horizontally: PropTypes.bool,
};