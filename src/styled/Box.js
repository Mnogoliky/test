import styled from 'styled-components';
import PropTypes from 'prop-types';

export const Box = styled.div`
	width: ${props => props.width};
	height: ${props => props.height};
	
	font-weight: 600;
	
	border-radius: 5px;
	background-color: white;
	box-shadow: 0px 0px 7px 0px rgba(0,0,0,0.5);
	
	padding: 13px;
	margin: 10px;
	box-sizing: border-box;
`;

Box.PropTypes = {
	width: PropTypes.string,
	height: PropTypes.string,
}