import styled from 'styled-components';
import PropTypes from 'prop-types'

export const Image = styled.img`
	width: ${props => props.size || '25px'};
	height: ${props => props.size || '25px'};
	margin: auto 0
`;

Image.propTypes = {
	size: PropTypes.number,
};