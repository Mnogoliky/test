import React, {Component} from 'react';
import {BigViget, LittleViget} from './components';
import {store} from './store';
import { getWeatherData } from './store/weather/actions';
import {Separator} from './styled';

export class App extends Component{
  componentDidMount(){
    store.dispatch(getWeatherData());
  }

  render(){
		return (
			<div className="App">
				<Separator style = {{width: '375px', height: '450px'}} horizontally>
					<BigViget/>
					<Separator>
						<LittleViget/>
						<LittleViget full/>
					</Separator>
				</Separator>
			</div>
		);
  }
}