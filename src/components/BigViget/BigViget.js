import React from 'react';
import { connect } from 'react-redux';
import { Box, Separator, Dimmer, Loader, Text, Image } from '../../styled';
import { getTimeFromDate, getDateStr } from '../../utils';

const BigVigetContainer = ({isLoad, error, data: {sys, weather, main, name}}) => (
	<Box width = '355px' height = '250px'>
		{
			isLoad ?
				(
					<Dimmer>
						<Loader/>
					</Dimmer>
				) :
				error ? (
						<Dimmer>
							<Text>Something went wrong</Text>
						</Dimmer>
					) :
					(
						<Separator style = { {height: '100%'} }>
							<Separator horizontally>
								<div>
									<Text size = '13px'>{ getDateStr() }</Text>
									<Text size = '20px'>{ weather[0].description }</Text>
								</div>
								<div>
									<Separator>
										<div>
											<Text size = '25px'>{ main.temp }°C</Text>
											<Text>{ name } { sys.country }</Text>
										</div>
										<Image size = '45px' src = { `https://openweathermap.org/img/wn/${weather[0].icon}@2x.png` }
													 alt = ""/>
									</Separator>
								</div>
							</Separator>
							<div>
								<Text size = '13px'>Pressure: { main.pressure } hpa</Text>
								<Text size = '13px'>Humidity: { main.humidity } %</Text>

								<Text size = '13px'>Sunrise: { getTimeFromDate(sys.sunrise) }</Text>
								<Text size = '13px'>Sunset: { getTimeFromDate(sys.sunset) }</Text>
							</div>
						</Separator>
					)
		}
	</Box>
);

export const BigViget = connect(state => ({
	isLoad: state.weatherReducer.isLoad,
	data: state.weatherReducer.data,
	error: state.weatherReducer.error,
}))(BigVigetContainer);