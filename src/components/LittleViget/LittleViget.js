import React from 'react';
import { connect } from 'react-redux';
import { Box, Separator, Dimmer, Loader, Text, Image } from '../../styled';
import { getDateStr } from '../../utils';
import PropTypes from 'prop-types';

const LittleVigetContainer = ({isLoad, error, full, data: {sys, weather, main, name}}) => (
	<Box width = '145px' height = '125px'>
		{
			isLoad ?
				(
					<Dimmer>
						<Loader/>
					</Dimmer>
				) :
				error ? (
						<Dimmer>
							<Text>Something went wrong</Text>
						</Dimmer>
					) :
					(
						<Separator horizontally>
							{ full ? <Text size = '13px'>{ getDateStr() }</Text> : <p/> }
							<div>
								<Separator>
									<div>
										<Text size = '20px'>{ main.temp }°C</Text>
										<Text>{ name } { sys.country }</Text>
									</div>
									<Image size = '30px' src = { `https://openweathermap.org/img/wn/${weather[0].icon}@2x.png` }
												 alt = ""/>
								</Separator>
							</div>
						</Separator>
					)
		}
	</Box>
);

export const LittleViget = connect(state => ({
	isLoad: state.weatherReducer.isLoad,
	data: state.weatherReducer.data,
	error: state.weatherReducer.error,
}))(LittleVigetContainer);

LittleViget.PropTypes = {
	full: PropTypes.bool,
};