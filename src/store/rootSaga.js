import { all } from 'redux-saga/dist/redux-saga-effects-npm-proxy.esm';
import { watchAllWeather } from './weather/saga';

export function* rootSaga() {
	yield all([
		watchAllWeather(),
	]);
}
