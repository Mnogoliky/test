import {createAction} from '../../utils';
const family = '@weather/';

export const setLoadStatus = createAction(family+'SET_LOAD_STATUS', status => ({status}));

export const getWeatherData = createAction(family+'GET_WEATHER_DATA', (data) => ({}));
export const setWeatherData = createAction(family+'SET_WEATHER_DATA', data => ({data}));

export const setError = createAction(family+'SET_ERROR', err => ({err}));