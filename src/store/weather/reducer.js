import * as actions from './actions';

const initialState = {
	isLoad: false,
	data: {
		main: {},
		sys: {},
		weather: [{}]
	},
	error: false,
};

export const weatherReducer = (state = initialState, action) => {
	return ({
		[actions.setLoadStatus]:
			{
				...state,
				isLoad: action.status,
			},
		[actions.setWeatherData]:
			{
				...state,
				data: action.data,
			},
		[actions.setError]:
			{
				...state,
				error: action.err,
			},

	})[action.type] || state;
};