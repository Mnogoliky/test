import {
	createStore,
	combineReducers,
	applyMiddleware,
	compose,
} from 'redux';

import {weatherReducer} from './weather/reducer';
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from './rootSaga';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const rootReducers = combineReducers({
	weatherReducer,
});
const sagaMiddleware = createSagaMiddleware();
export const store = createStore(rootReducers, composeEnhancers(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(rootSaga);